/*
 * author:bolide
 */
(function(){
	
	var audio = $("#hl_audio_player").get(0);
	
	if(!audio){
		throw new Error("please add a audio element and the id is hl_audio_player to body first!");
		$("body").html("<audio id='hl_audio_player'/>")
	}
	
	var option = {
		src : "",
		onTimeUpdate : function(){},
		onEnded : function(){}
	};
	
	audio.addEventListener("canplay", function(){
		audio.play();
	});
	
	audio.addEventListener("timeupdate", function(){
		option.onTimeUpdate();
	});
	
	audio.addEventListener("ended", function(){
		option.onEnded();
	});
	
	var player = {
		init : function(obj){
			$.extend(option, obj);
		},
		setOptionSrc : function(src){
			option.src = src;
		},
		getOptionSrc : function(){
			return option.src;
		},
		play : function(url){
			audio.src = url || option.src;
			audio.load();
		},
		pause : function(){
			audio.pause();
		},
		resume : function(){
			audio.play(audio.currentTime);
		},
		getDuration : function(){
			return audio.duration;
		},
		getCurrentTime : function(){
			return audio.currentTime;
		},
		setCurrentTime : function(currentTime){
			audio.currentTime = currentTime;
		},
		isPaused : function(){
			return audio.paused && audio.currentTime > 0;
		},
		getSrc : function(){
			return audio.src;
		},
		setVolume : function(volume){
			audio.volume = volume;
		}
	};
	
	window.MyAudioPlayer = player;
})();